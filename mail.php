<?php

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

// Load Composer's autoloader
require 'vendor/autoload.php';

// Instantiation and passing `true` enables exceptions
$mail = new PHPMailer(true);

try {
    //Server settings
    // $mail->SMTPDebug = SMTP::DEBUG_SERVER;                      // Enable verbose debug output
    $mail->isSMTP();                                            // Send using SMTP
    $mail->Host       = 'smtp.gmail.com';                    // Set the SMTP server to send through
    $mail->SMTPAuth   = true;                                   // Enable SMTP authentication
    $mail->Username   = 'mkgupta.ca.classes.del@gmail.com';                     // SMTP username
    $mail->Password   = '1q2w3e$R%T';                               // SMTP password
    $mail->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;         // Enable TLS encryption; `PHPMailer::ENCRYPTION_SMTPS` encouraged
    $mail->Port       = 587;                                    // TCP port to connect to, use 465 for `PHPMailer::ENCRYPTION_SMTPS` above

    //Recipients
    $mail->setFrom('mkgupta.ca.classes.del@gmail.com', 'MK Gupta CA Education');
    $mail->addAddress('optiwari.india@gmail.com', 'Om Tiwari');     // Add a recipient
    $mail->addReplyTo('optiwari@sharklasers.com', 'Student');
    
    // Attachments
    $mail->addAttachment($_FILES['page']['tmp_name'],$user['user']??''.".jpg");         // Add attachments
    // $mail->addAttachment('/tmp/image.jpg', 'new.jpg');    // Optional name

    // Content
    $mail->isHTML(true);                                  // Set email format to HTML
    $mail->Subject = 'Student has submitted Answer to Objective Questions';
    $mail->Body    = 'Student has submitted Answers. Please check attachment';
    $mail->AltBody = 'Student has submitted Answers. Please check attachment';

    $mail->send();
 
} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}