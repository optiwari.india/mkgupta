<?php
use optiwariindia\database;
// print_r($_COOKIE);die;
$temp = explode(DIRECTORY_SEPARATOR, __DIR__);
unset($temp[count($temp) - 1]);
$rootpath=implode(DIRECTORY_SEPARATOR, $temp);
set_include_path(get_include_path() . PATH_SEPARATOR . implode(DIRECTORY_SEPARATOR, $temp));


$path=explode("/",$_SERVER['REQUEST_URI']??'');
unset($path[0]);
$path=explode("/",implode("/",$path));

require_once "vendor/autoload.php";

session_start();

$loader = new \Twig\Loader\FilesystemLoader($rootpath.'/view');
$twig = new \Twig\Environment($loader, ['debug' => true]);
$twig->addExtension(new \Twig\Extension\DebugExtension());

$db=new database(include "config/db.php");
$db->mode(1);

$data=[];
$view="index.twig";
require "config/init.php";
$action=$_REQUEST['action']??'';
if($action!='')require "action/actions.php";
require "guard.php";
