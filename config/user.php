<?php

$data['user'] = $_SESSION['user'];
$path = array_map(function ($arr) {
    return urldecode($arr);
}, $path);
$user = $_SESSION['user'];
$data['books'] = $db->select("permissions", "book as title", "where user={$data['user']['id']}");
$data['course'] = $db->select("subject", "*", "where name like '{$user['subject']}'")[0] ?? [];
switch ($path[0]) {
    case '':
        $data['view'] = "default.twig";
        break;
    case "course":
        $data['course'] = $db->select("subject", "*", "where id ={$path[1]}")[0];
        $books = [];

        foreach ($db->select("book1", "subject,name,count(*) as pages", "where subject={$path[1]} group by 1,2") as $book) {
            // Checking permissions
            $db->debug();
            $temp = $db->select("permissions", "*", "where book like '{$book['name']}' and user={$user['id']}");
            if (count($temp) != 0) {
                $books[] = $book;
            } else {
                if(strtolower($user['subject'])=='foundation')
                $books[]=$book;
            }
        }
        $data['books'] = $books;

        $data['view'] = "course.twig";
        break;
    case "book":
        if (is_numeric($path[1])) {
            $data['book'] = $db->select("book", "*", "where id = {$path[1]}")[0];
            $data['view'] = "show.book.twig";
        } else {
            $data['pages'] = $db->select("book1", "id,name", "where name like '" . urldecode($path[1]) . "' order by page");
            $data['view'] = "show.book.img.twig";
            $data['pagination']['pages'] = count($data['pages']);
        }
        break;
    case "practice":
        switch ($path[1]) {
            case 'list':
                $temp = $db->select("subject", "id", "where name like '{$user['subject']}'")[0]['id'] ?? 0;
                $data['exams'] = $db->select("qp", "subject,title,type,count(*) as pages", "where subject={$temp} and category='practice' group by 1,2,3 order by 1,2,3");
                $data['view'] = "practice/list.twig";
                break;
            case "attempt":
                $data['qpinfo'] = $path;
                $data['qp'] = $db->select("qp", "id", "where category='practice' and subject={$path[2]} and title='" . urldecode($path[3]) . "' and type='{$path[4]}' order by filename");
                if ($path['4'] == 'objective') {
                    $temp = [];

                    foreach ($db->select("answer", "qno,ans", "where category='practice' and subject={$path[2]} and title='" . urldecode($path[3]) . "'") as $value) {
                        $temp[$value['qno']] = $value['ans'];

                    }
                    $data['ans'] = $temp;
                    // print_r($temp);

                }
                $data['view'] = "practice/attempt.twig";
                break;
            case "answers":
                $resp = $db->select("subans", "id", "where subject={$path[2]} and title='" . urldecode($path[3]) . "' order by filename");
                $data['ans'] = $resp;
                $data['view'] = "practice/answers.twig";

                break;
            default:
                # code...
                die("Please wait while loading answers");
                break;
        }
        break;
    case "moctest":
        switch ($path[1]) {
            case 'list':
                $temp = $db->select("subject", "id", "where name like '{$user['subject']}'")[0]['id'] ?? 0;
                $data['exams'] = $db->select("qp", "subject,title,type,count(*) as pages", "where subject={$temp} and category='moctest' group by 1,2,3 order by 1,2,3");
                $data['view'] = "mocktest/list.twig";
                break;
            case "attempt":
                $data['qpinfo'] = $path;
                $data['qp'] = $db->select("qp", "id", "where category='moctest' and subject={$path[2]} and title='" . urldecode($path[3]) . "' and type='{$path[4]}' order by filename");

                $data['view'] = "mocktest/attempt.twig";
                break;

            default:
                # code...
                break;
        }
        break;
    case "examinations":
        switch ($path[1]) {
            case 'list':
                $db->debug();
                $temp = $db->select("subject", "id", "where name like '{$user['subject']}'")[0]['id'] ?? 0;
                $data['exams'] = $db->select("exam_permission", "*", "where attempt!=0 and user={$user['id']} order by id");
                $data['view'] = "exams/list.twig";
                break;
            case "attempt":
                $db->debug();
                $data['qpinfo'] = $path;
                $db->update("exam_permission", ["attempt" => 0], "where id={$path[2]} and attempt!=0");
                $exam = $db->select("exam_permission", "*", "where id={$path[2]}")[0] ?? [];
                $data['qp'] = $db->select("qp", "id,duration", "where category='exam' and subject={$exam['subject']} and title='{$exam['title']}' and type='{$exam['type']}' order by filename");
                foreach($db->select("instructions","id,name","where category='exam' and subject={$exam['subject']} and title='{$exam['title']}' and type='{$exam['type']}'")as $val){
                    $data['info'][$val['name']]=$val['id'];
                }
                if ($exam['type'] != 'subjective') {
                    $temp = [];
                    foreach ($db->select("answer", "qno,ans", "where category='exam' and subject={$exam['subject']} and title='{$exam['title']}'") as $value) {
                        $temp[$value['qno']] = $value['ans'];
                    }
                    $data['ans'] = $temp;
                }
                switch ($exam['type'] ?? '') {
                    case 'subjective':
                        $data['objective'] = false;
                        $data['subjective'] = true;
                        break;
                    case 'objective':

                        $data['objective'] = true;
                        $data['subjective'] = false;
                        break;
                    case 'subjective+objective':

                        $data['objective'] = true;
                        $data['subjective'] = true;
                        break;
                    default:
                        # code...
                        break;
                }
                $data['view'] = "exams/attempt.twig";
                break;
            case "review":
                $data['pages']=$db->select("attempt","id","where exam={$path[2]}");
                $data['view']="exams/review.twig";
            break;
            case "delete":
                $db->delete("attempt","where id ={$path[3]} and exam={$path[2]}");
                header("Location: /examinations/review/{$path[2]}/{$path[4]}");
                
            break;
            default:
        }
        break;
    case "logout":
        session_destroy();
        header("Location: /");
        break;
    case "results":
        $data['results'] = $db->select("exam_permission", "*", "where user={$user['id']} and attempt=0 order by id desc");
        //$db->select("attempt", "subject,qp,category,date(added_on) as attemptDate,count(*) as pages", "where student={$user['id']}");
        $data['view'] = "results/list.twig";
        break;
    case "eval":
        $data['pages'] = $db->select("attempt", "id", "where exam={$path[1]}");
        $data['paper'] = $db->select("exam_permission", "*", "where id={$path[1]}");
        $data['view'] = 'results/eval.twig';
        $data['qpid'] = $path[1];
        $data['eval'] = $db->select("eval", "*", "where exam={$path[1]}");
        break;
    case "omr":
        $temp = $db->select("exam_permission", "*", "where id={$path[1]}")[0];
        $temp['type'] = str_replace("+", " ", $temp['type'] ?? '');
        $cans = array_map(function ($ans) {
            $tmp = explode(",", $ans['marks']);
            $can = [
                "qno" => $ans['qno'],
                "ans" => $ans['ans'],
                "co" => $tmp[0],
                "ua" => $tmp[1],
                "wr" => $tmp[2],
            ];
            return $can;
        }, $db->select("answer", "qno,ans,marks", "where category='{$temp['category']}' and subject='{$temp['subject']}' and title='{$temp['title']}' and type='{$temp['type']}'"));
        $db->mode(3);
        $data['cans'] = [];
        foreach ($cans as $value) {
            $data['cans'][$value['qno']] = $value;
        }

        $data['omr'] = [];
        $tmp = $db->select("objatt", "*", "where exam={$path[1]}")[0] ?? [];
        foreach ($tmp['omr'] as $val) {
            $data['omr'][$val['qno']] = $val['ans'];
        }
        $student = $db->select("auth", "*", "where id={$tmp['student']}");
        $data['student'] = $student[0] ?? [];
        $data['exam'] = $temp;

        $data['view'] = "results/omr.twig";
        break;

    default:

        print_r($path);die;
        break;
}
