<?php
$data['courses'] = $db->select("subject", "*", "order by name");
$data['qptype'] = [["name" => "subjective"], ["name" => "objective"], ["name" => "subjective+objective"]];
$path = array_map(function ($arr) {
    return urldecode($arr);
}, $path);
if (($path[2] ?? '') == "subjective objective") {
    $path[2] = "subjective+objective";
}

switch ($path[0]) {
    case '':
        $data['view'] = "default.twig";
        break;
    case "manage":
        switch ($path[1]) {
            case 'course':
                $data['view'] = "manage/course.twig";
                break;
            case "student":
                $data['students'] = $db->select("auth", "*", "where role='user' order by computer desc,id desc");
                $data['view'] = "manage/students.twig";
                break;
            default:
                # code...
                break;
        }
        break;
    case "course":
        switch ($path[1]) {
            case 'add':
                $data['view'] = "form/course.add.twig";
                break;
            case "del":
                $db->debug();
                $temp = $db->select("subject", "*", "where id={$path[2]}");
                $db->delete("auth", "where subject like '{$temp[0]['name']}'");
                $db->delete("book1", "where subject = {$path[2]}");
                $db->delete("subject", "where id={$path[2]}");

                header("Location: /manage/course");
                break;
            case "permissions":
                $data['subject'] = $db->select("subject", "*", "where id = {$path[2]}")[0] ?? [];
                $data['books'] = $db->select("book1", "distinct name", "where subject={$path[2]}");
                $temp = [];
                foreach ($db->select("permissions") as $value) {
                    $temp[$value['user']][$value['book']] = 1;
                }
                $data['permission'] = $temp;
                $data['students'] = $db->select("auth", "*", "where role='user' and subject like '{$data['subject']['name']}'");
                $data['view'] = "form/books.permissions.twig";
                break;
            case "grant":
                $db->insert("permissions", ["book" => urldecode($path[2]), "user" => $path[3], "added_from" => "web"]);
                header("Location: /course/permissions/{$path[4]}");
                die;

            case "revoke":
                $db->delete("permissions", "where book ='" . urldecode($path[2]) . "' and user = {$path[3]}");
                header("Location: /course/permissions/{$path[4]}");
                die;
            default:
                # code...
                break;
        }
        break;
    case "books":
        if (isset($path[1])) {
            if (is_numeric($path[1])) {
                $data['course'] = $db->select("subject", "*", "where id ={$path[1]}")[0];
                $data['book1'] = $db->select("book1", "subject,name,count(*) as pages", "where subject={$path[1]} group by 1,2");
                $data['view'] = "books/list.twig";
            } else {
                switch ($path[1]) {
                    case 'add':
                        $data['view'] = "form/course.add.twig";
                        break;
                    case "del":
                        $db->debug();
                        $temp = $db->select("subject", "*", "where id={$path[2]}");
                        $db->delete("auth", "where subject like '{$temp[0]['name']}'");
                        $db->delete("book1", "where subject = {$path[2]}");
                        $db->delete("subject", "where id={$path[2]}");
                        // header("Location: /manage/course");
                        die;
                        break;
                    default:
                        # code...
                        break;
                }
            }
        } else {

            header("Location: /");
        }
        break;
    case "book":
        switch ($path[1]) {
            case 'add':
                $data['subjectid'] = $path[2];
                $data['view'] = "form/book.add.twig";
                break;
            case "addimg":
                $data['subjectid'] = $path[2];
                $data['book'] = $_SESSION['book'] ?? '';
                if (isset($_SESSION['book'])) {
                    $data['pages'] = $db->select("book1", "id", "where name like '{$_SESSION['book']['name']}' and subject={$path[2]}");
                }

                $data['view'] = "form/book.addimg.twig";
                break;
            case "delete":
                $db->query("delete from book where id={$path[2]}");
                header("Location: /course/" . $path[3]);
                break;
            case "del":
                $db->query("delete from book1 where name like '" . urldecode($path[2]) . "' and subject={$path[3]}");
                header("Location: /course/" . $path[3]);
                break;
            default:
                $data['pages'] = $db->select("book1", "id,name", "where name like '" . urldecode($path[1]) . "' order by page");
                $data['view'] = "books/open.twig";
                break;
        }

        break;
    case "student":
        switch ($path[1]) {
            case 'add':
                $data['view'] = "form/student.add.twig";
                break;
            case 'addlist':
                $data['view'] = "form/student.addlist.twig";
                break;
            case "del":
                $db->query("delete from auth where id={$path[2]}");
                header("Location: /manage/student");
                break;
            default:
                # code...
                break;
        }
        break;
    case "logout":
        session_destroy();
        header("Location: /");
        break;
    case "qp":
        $data["subjectid"] = $path[2];
        $data['view'] = "form/qp.addimg.twig";

        break;
    case "practice":

        switch ($path['1']) {
            case 'list':
                $db->mode(2);
                $data['subject'] = $db->select("subject");
                $db->mode(1);
                $data['exams'] = $db->select("qp", "subject,title,type,count(*) as pages", "where category='practice' group by 1,2,3 order by 1,2,3");
                $data['view'] = "practice/list.twig";
                break;
            case "add":
                $data['qpcat'] = "practice";
                $data['view'] = "practice/add.twig";
                break;
            case "delete":
                $db->delete("qp", "where subject={$path[2]} and title like '" . urldecode($path[3]) . "' and type='{$path[4]}'");
                header("Location: /practice/list");
                die("Deleting please wait");
                break;
            case "answers":
                $data['qpinfo'] = $path;
                $data['quest'] = $_REQUEST['quest'] ?? '';

                $temp = $db->select("answer", "*", "where category='{$path[0]}' and subject={$path[2]} and title='" . urldecode($path[3]) . "' and type='{$path[4]}'");
                if (count($temp) != 0) {
                    $data['answers'] = $temp;
                }
                $data['view'] = "practice/ans/objective.twig";
                break;
            case "ansimg":

                $data['qpinfo'] = $path;
                $data['quest'] = $_REQUEST['quest'] ?? '';
                $temp = $db->select("subans", "id", "where category='subjective' and subject={$path[2]} and title='" . urldecode($path[3]) . "'");
                if (count($temp) != 0) {
                    $data['answers'] = $temp;

                }
                $data['view'] = "practice/ans/subjective.twig";
                break;
            case "open":
                $data['view'] = "practice/open.twig";
                $data['qp'] = $db->select("qp", "id", "where category='practice' and subject={$path[2]} and title='" . urldecode($path[3]) . "' and type='{$path[4]}' order by filename");
                break;
            default:
                # code...
                break;
        }
        break;
    case "moctest":
        switch ($path['1']) {
            case 'list':
                $db->mode(2);
                $data['subject'] = $db->select("subject");
                $db->mode(1);
                $temp = $db->select("qp", "subject,title,type,count(*) as pages", "where category='moctest' group by 1,2,3 order by 1,2,3");
                $data['exams'] = array_map(function ($exm) {
                    global $db;
                    $t = $db->select("mocans", "count(distinct student) as attempts", "where title like '{$exm['title']}' and subject={$exm['subject']}");
                    $exm['attempts'] = $t[0]['attempts'];
                    return $exm;
                }, $temp);

                $data['view'] = "mocktest/list.twig";
                break;
            case "add":
                $data['qpcat'] = "moctest";
                $data['view'] = "mocktest/add.twig";
                break;
            case "delete":
                $db->delete("qp", "where subject={$path[2]} and title like '" . urldecode($path[3]) . "' and type='{$path[4]}'");
                header("Location: /moctest/list");
                die("Deleting please wait");
                break;
            case "answers":
                $data['qpinfo'] = $path;
                $data['quest'] = $_REQUEST['quest'] ?? '';
                $data['view'] = "mocktest/ans/objective.twig";
                break;
            case "ansimg":
                $data['qpinfo'] = $path;
                $data['view'] = "mocktest/ans/subjective.twig";
                break;
            case "open":
                $data['view'] = "mocktest/open.twig";

                $data['qp'] = $db->select("qp", "id", "where category='moctest' and subject={$path[2]} and title='" . urldecode($path[3]) . "' and type='{$path[4]}'");

                break;
            default:
                # code...
                break;
        }
        break;

    case "examinations":
    case "exam":
        switch ($path['1']) {
            case 'list':
                $db->mode(2);
                $data['subject'] = $db->select("subject");
                $db->mode(1);
                $exam = $db->select("qp", "subject,title,type,count(*) as pages", "where category='exam' group by 1,2,3 order by 1,2,3");
                $data['exams'] = array_map(function ($e) {
                    global $db;
                    $db->debug();
                    $t = $db->select("exam_permission", "user,count(*) as pages", "where  subject={$e['subject']} and title='{$e['title']}' group by 1 ");
                    $e['attempts'] = count($t);
                    return $e;
                }, $exam);

                $data['view'] = "exams/list.twig";
                break;
            case "add":
                $data['qpcat'] = "exam";
                $data['view'] = "exams/add.twig";
                break;
            case "delete":
                $db->delete("qp", "where subject={$path[2]} and title like '" . urldecode($path[3]) . "' and type='{$path[4]}'");
                header("Location: /exam/list");
                die("Deleting please wait");
                break;

            case "answers":
                $data['qpinfo'] = $path;
                $r = $db->resa($_REQUEST);
                $data['quest'] = $_REQUEST['quest'] ?? '';
                $data['view'] = "exams/ans/objective.twig";
                switch ($r['page'] ?? 'objective') {
                    case "objective":
                        $temp = $db->select("answer", "*", "where category='{$path[0]}' and subject={$path[2]} and title='" . urldecode($path[3]) . "' and type='{$path[4]}'");
                        if (count($temp) != 0) {
                            $data['answers'] = array_map(function ($ans) {
                                $ans['marks'] = explode(",", $ans['marks']);
                                return $ans;
                            }, $temp);
                        }
                        $data['view'] = "exams/ans/objective.twig";
                        break;
                    case "subjective":
                        $data['view'] = "exams/ans/subjective.twig";
                        break;
                }
                switch ($path[4] ?? '') {
                    case "objective":
                        $data['show']['objective'] = true;
                        break;
                    case "subjective":
                        $fallback = "subjective";
                        $data['show']['subjective'] = true;
                        $data['view'] = "exams/ans/subjective.twig";
                        // die("Subjective");
                        break;
                    default:
                        $data['show']['subjective'] = true;
                        $data['show']['objective'] = true;
                        break;
                }
                break;
            case "marking":
                $data['qpinfo'] = $path;
                $fallback = "objective";
                switch ($path[4] ?? '') {
                    case "objective":
                        $data['show']['objective'] = true;
                        break;
                    case "subjective":
                        $fallback = "subjective";
                        $data['show']['subjective'] = true;
                        break;
                    default:
                        $data['show']['subjective'] = true;
                        $data['show']['objective'] = true;
                        break;
                }
                $r = $db->resa($_REQUEST);
                if (isset($r['sections'])) {
                    $data[$r['page']]['section'] = $r['sections'];
                }
                $data['page'] = $r['page'] ?? $fallback;
                // echo $r['page'];die;
                $data['view'] = "exams/marking.twig";
                break;
            case "open":
                $data['view'] = "exams/open.twig";
                $data['qp'] = $db->select("qp", "id", "where category='exam' and subject={$path[2]} and title='" . urldecode($path[3]) . "' and type='{$path[4]}'");
                break;
            case "permission":

                $db->mode(2);
                if ($path[4] == "subjective objective") {
                    $path[4] = "subjective+objective";
                }

                $data['qpinfo'] = $path;
                $data['users'] = $db->select("auth", "id,user,name,batch", "order by id desc");
                $data['permissions'] = $db->select("exam_permission", "user,attempt", "where category='exam' and subject={$path[2]} and title='" . urldecode($path[3]) . "' and type='{$path[4]}'");
                $data['view'] = "exams/permission.twig";
                //die;
                break;
            case "instructions":
                $data['qpinfo'] = $path;
                $data['view'] = "exams/instructions.twig";
                break;
            default:
                # code...
                break;
        }
        break;
    case 'logs':
        $db->mode(3);
        $data['logs'] = $db->select("logs", "*", "order by id desc");
        $data['view'] = 'logs.twig';
        break;
    case "results":
        $db->mode(2);
        $data['subject'] = $db->select("subject");
        $db->mode(1);
        $data['exams'] = $db->select("exam_permission", "subject,category,type,title,count(*) as attempts,count(marks) as evaluated", "where attempt=0 group by 1,2,3,4 order by added_on desc");
        $data['view'] = "results/list.twig";
        break;
    case "evaluate":
        $db->mode(2);
        $data['student'] = $db->select("auth", "*", "where role='user'");
        $db->mode(1);

        $data['papers'] = $db->select("exam_permission", "*", "where subject='{$path[1]}' and type='{$path[2]}' and category='{$path[3]}' and title='{$path[4]}'");
        $data['view'] = 'results/evlist.twig';
        break;
    case "eval":
        $data['pages'] = $db->select("attempt", "id", "where exam={$path[1]}");
        $data['paper'] = $db->select("exam_permission", "*", "where id={$path[1]}");
        $data['view'] = 'results/eval.twig';
        $data['qpid'] = $path[1];
        $data['eval'] = $db->select("eval", "*", "where exam={$path[1]}");
        break;
    case "omr":
        $temp = $db->select("exam_permission", "*", "where id={$path[1]}")[0];
        $temp['type'] = str_replace("+", " ", $temp['type'] ?? '');
        $cans = array_map(function ($ans) {
            $tmp = explode(",", $ans['marks']);
            $can = [
                "qno" => $ans['qno'],
                "ans" => $ans['ans'],
                "co" => $tmp[0],
                "ua" => $tmp[1],
                "wr" => $tmp[2],
            ];
            return $can;
        }, $db->select("answer", "qno,ans,marks", "where category='{$temp['category']}' and subject='{$temp['subject']}' and title='{$temp['title']}' and type='{$temp['type']}'"));
        $db->mode(3);
        $data['cans'] = [];
        foreach ($cans as $value) {
            $data['cans'][$value['qno']] = $value;
        }

        $data['omr'] = [];
        $tmp = $db->select("objatt", "*", "where exam={$path[1]}")[0] ?? [];
        foreach ($tmp['omr'] as $val) {
            $data['omr'][$val['qno']] = $val['ans'];
        }
        $student = $db->select("auth", "*", "where id={$tmp['student']}");
        $data['student'] = $student[0] ?? [];
        $data['exam'] = $temp;

        $data['view'] = "results/omr.twig";
        break;
    case "students":
        switch ($path[1]) {
            case "list":
                $temp=$db->select("allowed_copies","*","where teacher={$user['id']}");
                $scripts=[];
                foreach($temp as $copy){
                    $exam=$db->select("exam_permission","*","where id={$copy['examno']}")[0];
                    $student=$db->select("auth","*","where id={$exam['user']}");
                    $anscopy=array_map(function($a){return $a['id'];},$db->select("attempt","id","where exam={$copy['examno']}"));
                    $scripts[]=[
                        "student"=>$student[0]??'',
                        "exam"=>$exam,
                        "copy"=>$anscopy,
                        "others"=>$copy
                    ];
                }
                $data['scripts']=$scripts;
                // header("content-type: text/txt");                print_r($scripts);                die;
              $data['view']="students/list.twig";
                break;
            case "open":
                break;
            case "review":
                break;
        }
        break;
    default:
        print_r($path);die;
        break;
}
