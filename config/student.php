<?php

$data['courses'] = $db->select("subject", "*", "order by name");
switch ($path[0]) {
    case '':
        $data['view'] = "default.twig";
        break;
    case "course":
        if (isset($path[1])) {
            if (is_numeric($path[1])) {
                $data['course'] = $db->select("subject", "*", "where id ={$path[1]}")[0];
                $data['books'] = $db->select("book", "*", "where subject={$path[1]}");
                $data['batches'] = $db->select("access a, groups g", "g.name", "where a.grpid=g.id and a.subject={$path[1]}");
            } else {

            }
        }

        $data['view'] = "course.twig";
        break;
    case "batch":
        $data['batches'] = $db->select("groups");
        if (is_numeric($path[1] ?? '')) {
            $temp = $db->select("groups", "students,name,id", "where id={$path[1]}");
            $data['batch'] = $temp[0];
            $data['students'] = $db->select("auth", "*", "where id in ({$temp[0]['students']})");
            $data['view'] = "batch.twig";
        } else {
            if (($path[1] ?? '') == 'add') {
                $data['subject'] = $db->select("subject");
                $data['view'] = "form/batch.add.twig";
            } else {
                $data['view'] = 'batch.twig';
            }
        }

        break;
    case "book":
        if (is_numeric($path[1])) {
            $data['book'] = $db->select("book", "*", "where id = {$path[1]}")[0];
            $data['view'] = "show.book.twig";
        } else {
            switch ($path[1]) {
                case 'add':
                    $data['subjectid'] = $path[2];
                    $data['view'] = "form/book.add.twig";
                    break;

                default:
                    # code...
                    break;
            }
        }
        break;
    case "student":
        $data['batch'] = $db->select("groups");
        $data['students'] = $db->select("auth", "*", "where role='user'");
        if (is_numeric($path[1] ?? '')) {
            $data['student'] = $db->select("auth", "*", "where id={$path[1]}")[0] ?? [];
            $data['view'] = "student.twig";
        } else {
            if (($path[1] ?? '') == "add") {
                $data['view'] = "form/student.add.twig";
            } else {
                $data['view'] = "student.twig";
            }
        }

        break;
    case "logout":
        session_destroy();
        header("Location: /");
        break;
    case "eval":
        $data['pages'] = $db->select("attempt", "id", "where exam={$path[1]}");
        $data['paper'] = $db->select("exam_permission", "*", "where id={$path[1]}");
        $data['view'] = 'results/eval.twig';
        $data['qpid'] = $path[1];
        $data['eval'] = $db->select("eval", "*", "where exam={$path[1]}");
        break;
    case "omr":
        $temp = $db->select("exam_permission", "*", "where id={$path[1]}")[0];
        $temp['type'] = str_replace("+", " ", $temp['type'] ?? '');
        $cans = array_map(function ($ans) {
            $tmp = explode(",", $ans['marks']);
            $can = [
                "qno" => $ans['qno'],
                "ans" => $ans['ans'],
                "co" => $tmp[0],
                "ua" => $tmp[1],
                "wr" => $tmp[2],
            ];
            return $can;
        }, $db->select("answer", "qno,ans,marks", "where category='{$temp['category']}' and subject='{$temp['subject']}' and title='{$temp['title']}' and type='{$temp['type']}'"));
        $db->mode(3);
        $data['cans'] = [];
        foreach ($cans as $value) {
            $data['cans'][$value['qno']] = $value;
        }

        $data['omr'] = [];
        $tmp = $db->select("objatt", "*", "where exam={$path[1]}")[0] ?? [];
        foreach ($tmp['omr'] as $val) {
            $data['omr'][$val['qno']] = $val['ans'];
        }
        $student = $db->select("auth", "*", "where id={$tmp['student']}");
        $data['student'] = $student[0] ?? [];
        $data['exam'] = $temp;

        $data['view'] = "results/omr.twig";
        break;

    default:
        print_r($path);die;
        break;
}
