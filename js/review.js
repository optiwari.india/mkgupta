let subtime = location.pathname.split("/")[location.pathname.split("/").length - 1];
let subtimer = setInterval(function() {
    subtime--;
    var min = Math.floor(subtime / 60);
    var sec = subtime % 60;
    sec = (sec < 10) ? "0" + sec : sec;
    document.querySelector(".time").innerHTML = "Upload time: " + min + ":" + sec;
    if (subtime < 1) {
        location.href = '/results/list';
    }
}, 1000);

function deletepage(id) {
    location.href = "/examinations/delete/" + location.pathname.split("/")[location.pathname.split("/").length - 2] + "/" + id + "/" + subtime;
}

function finishExam() {
    if (confirm("Do you want to submit your answer script")) {
        location.href = "/results/list";
    }
}