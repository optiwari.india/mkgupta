/*
filedrag.js - HTML5 File Drag & Drop demonstration
Featured on SitePoint.com
Developed by Craig Buckler (@craigbuckler) of OptimalWorks.net
*/
(function() {

    // getElementById
    function $id(id) {
        return document.getElementById(id);
    }


    // output information
    function Output(msg) {
        var m = $id("messages");
        m.innerHTML = msg + m.innerHTML;
    }


    // file drag hover
    function FileDragHover(e) {
        e.stopPropagation();
        e.preventDefault();
    }


    // file selection
    function FileSelectHandler(e) {
        FileDragHover(e);
        var files = e.target.files || e.dataTransfer.files;
        for (var i = 0, f; f = files[i]; i++) {
            ParseFile(f);
            UploadFile(f);
        }
    }

    function ParseFile(file) {

        if (file.type.indexOf("image") == 0) {
            var reader = new FileReader();
            reader.onload = function(e) {
                Output(
                    '<div class=page><img src="' + e.target.result + '" /></div>'
                );
            }
            reader.readAsDataURL(file);
        }

        // display text
        if (file.type.indexOf("text") == 0) {
            var reader = new FileReader();
            reader.onload = function(e) {
                Output(
                    "<p><strong>" + file.name + ":</strong></p><pre>" +
                    e.target.result.replace(/</g, "&lt;").replace(/>/g, "&gt;") +
                    "</pre>"
                );
            }
            reader.readAsText(file);
        }

    }

    function UploadFile(file) {
        var xhr = new XMLHttpRequest();
        if (xhr.upload && (file.type == "image/jpeg" || file.type == "image/png")) {
            var o = $id("progress");
            var progress = o.appendChild(document.createElement("p"));
            progress.appendChild(document.createTextNode("uploading " + file.name));
            submitButton = document.querySelector("#submit-subjective");
            xhr.upload.addEventListener("progress", function(e) {
                var pc = parseInt(100 - (e.loaded / e.total * 100));
                progress.style.backgroundPosition = pc + "% 0";
                if (e.total == e.loaded) {
                    progress.style.transition = "4s";
                    progress.style.display = "none";
                    if (location.pathname.startsWith("/examinations/list")) {
                        // alert("Uploaded");
                        location.reload();
                    }
                    // if (location.pathname.startsWith("/examinations/review")) {
                    //     console.log("This function called");
                    //     console.log(subtime);
                    //     location.href = "/examinations/review/" + location.pathname.split("/")[location.pathname.split("/").length - 2] + "/" + subtime;
                    // }
                    if (submitButton != null) {
                        submitButton.style = "display:block";
                    }
                } else {
                    if (submitButton != null) {
                        submitButton.style = "";
                    }
                }
            }, false);

            // file received/failed
            xhr.onreadystatechange = function(e) {
                if (xhr.readyState == 4) {
                    progress.className = (xhr.status == 200 ? "success" : "failure");
                }
            };
            let formData = new FormData();
            inp = {
                name: document.querySelector("[name=name]"),
                subject: document.querySelector("[name=subject]"),
                action: document.querySelector("[name=action]"),
                type: document.querySelector("[name=type]"),
                cat: document.querySelector("[name=category]"),
                duration: document.querySelector("[name=duration]")
            }

            xhr.open("POST", "/", true);
            xhr.setRequestHeader("X_FILENAME", file.name);
            formData.append("page", file);
            if (inp.name != null) {
                formData.append("name", inp.name.value);
            }
            if (inp.subject != null) {
                formData.append("subject", inp.subject.value);
            }
            formData.append("type", inp.type.value);
            if (inp.duration != null) {
                formData.append("duration", inp.duration.value);
            }
            formData.append("path", location.pathname);
            if (inp.cat != null) {
                formData.append("category", inp.cat.value);
            }
            formData.append("action", inp.action.value);

            console.log(formData);
            xhr.send(formData);

        }

    }


    // initialize
    function Init() {

        var fileselect = $id("fileselect"),
            filedrag = $id("filedrag"),
            submitbutton = $id("submitbutton");

        // file select
        fileselect.addEventListener("change", FileSelectHandler, false);

        // is XHR2 available?
        var xhr = new XMLHttpRequest();
        if (xhr.upload) {

            // file drop
            filedrag.addEventListener("dragover", FileDragHover, false);
            filedrag.addEventListener("dragleave", FileDragHover, false);
            filedrag.addEventListener("drop", FileSelectHandler, false);
            // filedrag.style.display = "block";

            // remove submit button
            if (submitbutton != null)
                submitbutton.style.display = "none";
        }

    }

    // call initialization file
    if (window.File && window.FileList && window.FileReader) {
        Init();
    }


})();

function checkName() {
    let bookname = document.querySelector("[name=name]");
    if (bookname.validity.valid) {
        let filedr = document.querySelector("#filedrag");
        filedr.style = "display:block";
    } else {
        let filedr = document.querySelector("#filedrag");
        filedr.style = "display:none";
    }
}
checkName();

function fn() {
    let inp = document.querySelectorAll("[name]");
    inp.forEach(elm => {
        elm.addEventListener("keyup", (event) => {
            checkName();
        })
    })
};
fn();