var sidebar = {
    show: function() {
        document.querySelectorAll(".page-wrapper").forEach(function(elm) {
            document.querySelectorAll(".page-wrapper").forEach(function(elm) {
                elm.classList.add("toggled");
            })
        })
    },
    hide: function() {

        document.querySelectorAll(".page-wrapper").forEach(function(elm) {
            elm.classList.remove("toggled");
        })

    }
}
var zoom = {
    size: 100,
    in: function() {
        if (zoom.size < 250)
            zoom.size += 10;
        zoom.update();
    },
    out: function() {
        if (zoom.size > 50)
            zoom.size -= 10;
        zoom.update();
    },
    update: function() {
        document.querySelectorAll(".page img").forEach(function(page) {
            page.style = "width:" + zoom.size + "%";
        })
    }
}

function myScroller() {
    document.querySelectorAll(".main-section-scroll").forEach(function(elm) {
        if (location.pathname.startsWith("/book"))
            elm.style = "max-height:" + (innerHeight - 90) + "px;"
    })
}
myScroller();
if (location.pathname.startsWith("/book") || location.pathname.endsWith("objective")) {
    sidebar.hide();
}