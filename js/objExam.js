let element = document.querySelectorAll("td>span.option");
element.forEach(function(elm) {
    elm.addEventListener("click", function(ev) {
        let gp = this.parentElement.parentElement;
        if (!gp.hasAttribute("evaluated")) {
            el = gp.querySelectorAll("span");
            el.forEach(function(e) {
                e.style = "";
                e.removeAttribute("selected");
            });
            this.style = "background:#555;color:#fff";
            this.setAttribute("selected", true);
        }
    })
});


function submitAnswers() {
    var xhr = new XMLHttpRequest();
    xhr.onreadystatechange = function(e) {
        if (xhr.readyState == 4) {
            if (xhr.status == 200) {
                let omr = document.querySelector(".omr");
                omr.style = "display:none";
                if (paper.objective && !paper.subjective) {
                    location.href = "/results/list";
                }
                paper.objective = false;
            } else {
                alert("Check your interent connection and try again");
            }
        }
    };
    xhr.open("POST", "/xhr.php");
    xhr.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    attempts = document.querySelectorAll("[selected]");
    var ans = [];
    attempts.forEach(function(e) {

        ans.push({
            qno: e.parentElement.parentElement.querySelectorAll("th")[0].innerHTML,
            ans: e.innerHTML
        });
    });
    frminputs = {
        path: location.pathname,
        ans: ans,
        action: 'exam-obj-submitted'
    }
    xhr.send(JSON.stringify(frminputs));
}