var ajax = {
    post: function(data, url, callback) {
        var xhr = new XMLHttpRequest();
        xhr.onreadystatechange = callback;
        xhr.open("POST", url);
        xhr.send(JSON.stringify(data));
    }
}

var book = {
    rename: function(e) {
        let renfrm = document.querySelector("[frm=rename]");
        renfrm.style = "display:block";
        renfrm.querySelector("[name=oldname]").value = e;
        renfrm.querySelector("[data=oldname]").innerHTML = e;
    },
    close: function() {
        let renfrm = document.querySelector("[frm=rename]");
        renfrm.style = "display:none";
    }
}

var permission = {
    user: function() {
        document.querySelector("[frm=batch]").style = "";
        let frm = document.querySelector("[frm=indvpermission]");
        frm.style = "display:block";
    },
    batch: function() {
        let frm = document.querySelector("[frm=batch]");
        document.querySelector("[frm=indvpermission]").style = "";
        frm.style = "display:block";
    }
}