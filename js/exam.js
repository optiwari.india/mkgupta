function startExam() {
    document.querySelector(".exampopup").style = "display:none";

    var timer = setInterval(function() {
        timeleft--;
        var min = Math.floor(timeleft / 60);
        var sec = timeleft % 60;
        sec = (sec < 10) ? "0" + sec : sec;
        document.querySelector(".time").innerHTML = "Time Remaining: " + min + ":" + sec;
        if (timeleft < 1) {
            clearInterval(timer);
            if (paper.objective)
                submitAnswers();
            document.querySelector(".main-section-scroll").innerHTML = "<div class='page'><img class='lazy' data-src=" + timeoutimg + " alt=''></div>";
            if (paper.subjective) {

                let subtimer = setInterval(function() {
                    subtime--;
                    var min = Math.floor(subtime / 60);
                    var sec = subtime % 60;
                    sec = (sec < 10) ? "0" + sec : sec;
                    document.querySelector(".time").innerHTML = "Upload Answer in : " + min + ":" + sec;
                    if (subtime < 1) {
                        location.href = '/results/list';
                    }
                }, 1000)
            }
        }
    }, 1000);
}

function showResults() {
    if (paper.objective) {
        submitAnswers();
    }
    // paper.subjective = false;
    location.href = location.href = '/examinations/review/' + location.pathname.split("/")[location.pathname.split("/").length - 1] + "/" + subtime;

}