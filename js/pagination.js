var book = {
    pages: document.querySelectorAll(".page"),
    currentPage: 1,
    nextPage: function() {
        book.currentPage++;
        book.showPage(book.currentPage);
    },
    prevPage: function() {
        book.currentPage--;
        book.showPage(book.currentPage);
    },
    showPage: function(p) {
        book.pages[p].parentElement.scrollTop = book.pages[p].offsetTop;
    },
    gotoPage: function() {
        p = document.querySelector("[name=page]").value;
        if (p <= book.pages.length) {
            book.showPage(p - 1);
        } else {
            alert("This book has only " + book.pages.length + " pages");
        }
    }
}