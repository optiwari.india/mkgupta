<?php
require "vendor/autoload.php";
$db=new optiwariindia\database(include "config/db.php");
$r=json_decode(file_get_contents('php://input'),1);
session_start();
$path=explode("/",$r['path']);
$db->mode(1);
$exam=$db->select("exam_permission","*","where id={$path[3]}")[0]??[];
switch ($path[1]) {
  case 'examinations':
    $type="exam";
    # code...
    break;
  case "moctest":
    $type="moctest";
  break;
  default:
    # code...
    break;
}
$ans=array_map(function($arr){
  $marks=explode(",",$arr['marks']);
  $temp=[
    "qno"=>$arr['qno'],
    "ans"=>$arr['ans'],
    "marksc"=>$marks[0],
    "marksu"=>$marks[1],
    "marksw"=>$marks[2]
  ];
  
  return $temp;
  
},$db->select("answer","*","where category='exam' and subject={$exam['subject']} and title='{$exam['title']}'"));
$temp=[];
foreach($ans as $value){
  $temp[$value['qno']]=$value;
}
$ans=$temp;
$marks=0;
foreach ($r['ans'] as $value) {
  $tmp=$ans[$value['qno']];
  
  if($tmp['ans']==$value['ans']){
    $marks+=$tmp['marksc'];
  }else{
    $marks+=$tmp['marksw'];
  }
}
$attempt=[
  "student"=>$_SESSION['user']['id'],
  "omr"=>$r['ans'],
  "marks"=>$marks,
  "exam"=>$path[3]
];
if($exam['type']=='objective')
$db->update("exam_permission",["marks"=>$marks],"where id={$exam['id']}");
$db->insert("objatt",$attempt);