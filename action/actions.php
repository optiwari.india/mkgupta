<?php

$r = $db->resa($_REQUEST);
$dev = $_COOKIE['serial'] ?? '';
$user = $_SESSION['user'] ?? '';
$path = explode("/", $r['path']);
$path = array_map(function ($arr) {
    return urldecode($arr);
}, $path);

if ($dev != '') {
    $data['userinfo'] = $db->select("auth", "*", "where computer like '{$dev}'")[0] ?? [];
}
switch ($r['action']) {
    case 'login':
        $temp = $db->select("auth", "*", "where user='{$r['user']}' and pass=md5('{$r['pass']}')");
        if (count($temp) == 1) {
            $user = $temp[0];
            unset($user['pass']);
            if ($user['role'] == 'user') {

                if ($user['computer'] == '') {
                    $db->update("auth", ["computer" => $dev], "where id={$user['id']}");
                    $_SESSION['user'] = $user;
                    header("Location: /");
                } else {
                    if ($user['computer'] == $dev) {
                        $_SESSION['user'] = $user;
                        header("Location: /");
                    } else {
                        $data['error'] = ["heading" => "Authentication failed", "body" => "Your username and password is not matching with our records"];
                        echo $twig->render("login.twig", $data);
                        die;
                    }
                }
            } else {
                $_SESSION['user'] = $user;
                header("Location: /");
            }

        } else {
            $data['error'] = ["heading" => "Authentication failed", "body" => "Your username and password is not matching with our records"];
            echo $twig->render("login.twig", $data);
        }
        break;
    case "add-ans":
        $info = [
            "category" => $r['category'],
            "subject" => $r['subject'],
            "title" => urldecode($r['title']),
            "type" => $r['type'],
            "updated_from" => $_SERVER['REMOTE_ADDR'],
        ];
        $i = 1;

        while (isset($r['q' . $i])) {
            $info['qno'] = $i;
            $info['ans'] = $r['q' . $i];
            $info['marks'] = $r['coq' . $i] ?? '';
            $info['marks'] .= "," . $r['uaq' . $i] ?? '';
            $info['marks'] .= "," . $r['wrq' . $i] ?? '';
            if ($info['ans'] != '') {
                $db->insert("answer", $info);
            }

            $i++;

        }
        header("Location: /{$info['category']}/answers/{$info['subject']}/{$info['title']}/{$info['type']}");
        break;
    case "update-ans":
        $info = [
            "category" => $r['category'],
            "subject" => $r['subject'],
            "title" => urldecode($r['title']),
            "type" => $r['type'],
            "updated_from" => $_SERVER['REMOTE_ADDR'],
        ];
        $i = 1;

        while (isset($r['q' . $i])) {
            $info['qno'] = $i;
            $info['ans'] = $r['q' . $i];
            $info['marks'] = $r['coq' . $i] ?? '';
            $info['marks'] .= "," . $r['uaq' . $i] ?? '';
            $info['marks'] .= "," . $r['wrq' . $i] ?? '';
            if ($info['ans'] != '') {
                //$db->insert("answer", $info);
                $temp = $db->update("answer", $info, "where qno={$i} and category='{$r['category']}' and subject='{$r['subject']}'
                and title='{$r['title']}'
                and type='{$r['type']}'
                ");

            }

            $i++;

        }
        header("Location: /{$info['category']}/answers/{$info['subject']}/{$info['title']}/{$info['type']}");
        break;
    case "ans-update":
        $info = [
            "category" => $r['category'],
            "subject" => $r['subject'],
            "title" => urldecode($r['title']),
            "type" => $r['type'],
            "updated_from" => $_SERVER['REMOTE_ADDR'],
        ];
        $i = 1;

        while (isset($r['q' . $i])) {
            $info['qno'] = $i;
            $info['ans'] = $r['q' . $i];
            $info['marks'] = $r['coq' . $i] ?? '';
            $info['marks'] .= "," . $r['uaq' . $i] ?? '';
            $info['marks'] .= "," . $r['wrq' . $i] ?? '';
            if ($info['ans'] != '') {

                $temp = $db->select("answer", "*", "where qno={$i} and category='{$r['category']}' and subject='{$r['subject']}'
                and title='{$r['title']}'
                and type='{$r['type']}'
                ");
                if (count($temp) == 0) {
                    $db->insert("answer", $info);

                }
                $temp = $db->update("answer", $info, "where qno={$i} and category='{$r['category']}' and subject='{$r['subject']}'
                and title='{$r['title']}'
                and type='{$r['type']}'
                ");

            }

            $i++;

        }
        // print_r($r);
        // print_r($info);
        header("Location: /{$info['category']}/answers/{$info['subject']}/{$info['title']}/{$info['type']}");

        break;
    case "permit-book-batch":

        $book = explode("/", $r['path'])[1] ?? "";
        foreach ($db->select("auth", "id", "where role='user' and batch like '{$r['batch']}'") as $usr) {
            $priv = [
                "book" => $book,
                "user" => $usr['id'],
                "added_from" => $_SERVER['REMOTE_ADDR'],
            ];

            $t = $db->select("permissions", "*", "where book='{$book}' and user={$usr['id']}");
            if (count($t) == 0) {
                $db->insert("permissions", $priv);
            } else {
                $db->update("permissions", $priv, "where id={$t[0]['id']}");
            }
        }
        header("Location: /" . $r['path']);
        break;
    case "permit-book-user":
        $book = explode("/", $r['path'])[1] ?? "";
        foreach (explode("\n", str_replace(["\\r", "\\n", ",", ";", "\t"], "\n", $r['users'])) as $usr) {
            $temp = $db->select("auth", "id", "where user like '{$usr}'");
            if (count($temp) == 1) {
                $priv = [
                    "book" => $book,
                    "user" => $temp[0]['id'],
                    "added_from" => $_SERVER['REMOTE_ADDR'],
                ];

                $t = $db->select("permissions", "*", "where book='{$book}' and user={$temp[0]['id']}");
                if (count($t) == 0) {
                    $db->insert("permissions", $priv);
                } else {
                    $db->update("permissions", $priv, "where id={$t[0]['id']}");
                }
            } else {

            }
        }

        header("Location: /" . $r['path']);
        break;
    case "permit-exam-batch":

        foreach ($db->select("auth", "id", "where role='user' and batch like '{$r['batch']}'") as $usr) {
            $priv = [
                "category" => $r['category'],
                "subject" => $r['subject'],
                "title" => $r['title'],
                "type" => $r['type'],
                "user" => $usr['id'],
                "attempt" => 1,
            ];
            $t = $db->select("exam_permission", "*", "where category='{$r['category']}' and subject='{$r['subject']}' and title='{$r['title']}' and type='{$r['type']}' and user='{$usr['id']}' ");
            if (count($t) == 0) {
                $db->insert("exam_permission", $priv);
            } else {
                $db->update("exam_permission", $priv, "where category='{$r['category']}' and subject='{$r['subject']}' and title='{$r['title']}' and type='{$r['type']}' and user='{$usr['id']}' ");
            }
        }
        header("Location: /" . $r['path']);
        break;
    case "permit-exam-user":
        foreach (explode("\n", str_replace(["\\r", "\\n", ",", ";", "\t"], "\n", $r['users'])) as $usr) {
            $temp = $db->select("auth", "id", "where user like '{$usr}'");
            if (count($temp) == 1) {
                $priv = [
                    "category" => $r['category'],
                    "subject" => $r['subject'],
                    "title" => $r['title'],
                    "type" => $r['type'],
                    "user" => $temp[0]['id'],
                    "attempt" => 1,
                ];
                $t = $db->select("exam_permission", "*", "where category='{$r['category']}' and subject='{$r['subject']}' and title='{$r['title']}' and type='{$r['type']}' and user='{$temp[0]['id']}' ");
                if (count($t) == 0) {
                    $db->insert("exam_permission", $priv);
                } else {
                    $db->update("exam_permission", $priv, "where category='{$r['category']}' and subject='{$r['subject']}' and title='{$r['title']}' and type='{$r['type']}' and user='{$temp[0]['id']}' ");
                }
            }
        }

        header("Location: /" . $r['path']);
        break;

    case "add-book":
        $book = [
            "subject" => $r['subject'],
            "name" => $r['name'],
            "body" => $r['body'],
            "added_from" => $_SERVER['REMOTE_ADDR'],
        ];
        $resp = $db->insert("book", $book);
        if ($resp['result'] == 1) {
            header("Location: /book");
        }
        break;
    case "user-update":
        $user = [
            "name" => $r['name'],
            "email" => $r['email'],
            "phone" => $r['phone'],
        ];
        $temp = $db->update("auth", $user, "where id={$_SESSION['user']['id']}");
        header("Location: /");
        break;

    case "add-teacher":
        $teacher = [
            "user" => $r['user'],
            "pass" => $r['pass'],
            "name" => $r['name'],
            "email" => $r['email'],
            "phone" => $r['phone'],
            "role" => "teacher",
            "computer" => '',
            "subject" => $r['subject'],
            "added_from" => $_SERVER['REMOTE_ADDR'],
        ];
        //Checking if user does not exist
        $temp = $db->select("auth", "count(*) as count", "where user like '{$r['user']}'");

        if ($temp[0]['count'] > 0) {
            echo "User already exists";
            die;
        }
        $db->debug();
        $temp = $db->insert("auth", $teacher);
        header("Location: /manage/teachers");
        break;
    case "add-student":
        $student = [
            "user" => $r['user'],
            "pass" => $r['pass'],
            "name" => $r['name'],
            "email" => "",
            "phone" => $r['phone'],
            "role" => "user",
            "subject" => $r['subject'],
            "added_from" => $_SERVER['REMOTE_ADDR'],
        ];
        $temp = $db->insert("auth", $student);
        header("Location: /manage/student");
        break;
    case "change-passwd":
        $temp = $db->select("auth", "*", "where id={$_SESSION['user']['id']}");
        if (count($temp) == 1) {
            $db->update("auth", ["pass" => $r['newpass']], "where id = {$_SESSION['user']['id']}");
        }
        header("Location: /");
        break;
    case "add-book-img":
        $image = $_FILES['page'];
        $mime = explode("/", $_FILES['page']['type']);
        if ($mime[0] == 'image') {
            $brand = [
                "name" => $r['name'],
                "subject" => $r['subject'],
                "page" => $image['name'],
                "image" => addslashes(file_get_contents($image['tmp_name'])),
                "mime" => $mime[0] . "/" . $mime[1],
                "added_from" => $_SESSION['user']['id'],
            ];
            $db->debug();

            $db->insert("book1", $brand);
            $_SESSION['book'] = [
                "name" => $brand['name'],
            ];
        } else {
            header("HTTP/1.1 404 Operation Failed");
        }
        // header("Location: /book/addimg/".$r['subject']);
        break;
    case "student-upload":
        $file = $_FILES['studlist'];

        $data = [];
        foreach (explode("\n", file_get_contents($file['tmp_name'])) as $info) {
            $data[] = str_getcsv($info);
        }
        $flds = $data[0];
        $students = [];
        foreach ($data as $k => $v) {
            $temp = [];
            if ($k > 0) {
                foreach ($flds as $key => $value) {

                    $temp[$value] = $v[$key] ?? '';
                }
                if (($temp['user'] ?? '') != '') {
                    $temp['role'] = "user";
                    $temp['pass'] = $temp['phone'];
                    $temp['added_from'] = $_SERVER['REMOTE_ADDR'];
                    $temp['email'] = '';

                    $db->insert("auth", $temp);
                }
            }
        }
        header("Location: /manage/student");

        break;
    case "add-course":
        $db->insert("subject", ["name" => $r['name'], "added_from" => $_SERVER['REMOTE_ADDR']]);
        header("Location: /manage/course");
        break;
    case "add-qp-img":

        $file = $_FILES['page'];
        $mime = explode("/", $file['type']);
        if ($mime[0] == "image") {

            $data = [
                "category" => $r['category'],
                "subject" => $r['subject'],
                "title" => $r['name'],
                "type" => $r['type'],
                "filename" => $file['name'],
                "image" => addslashes(file_get_contents($file['tmp_name'])),
                "duration" => $r['duration'] ?? '',
                "mime" => $file['type'],
                "added_from" => $_SERVER['REMOTE_ADDR'],
            ];
            $db->debug();

            print_r($db->insert("qp", $data));
        }
        break;

    case "upload-instructions":
        $file = $_FILES['page'];
        $mime = explode("/", $file['type']);

        if ($mime[0] == "image") {
            $data = [
                "category" => "exam",
                "subject" => $r['subject'],
                "title" => $r['name'],
                "type" => $r['type'],
                "name" => "instruction",
                "image" => addslashes(file_get_contents($file['tmp_name'])),
                "mime" => $file['type'],
                "added_by" => $user['id'],
            ];
            $db->insert("instructions", $data);
            header("Location: {$r['path']}");
        }

        break;
    case "upload-timeout":
        $file = $_FILES['page'];
        $mime = explode("/", $file['type']);

        if ($mime[0] == "image") {
            $data = [
                "category" => "exam",
                "subject" => $r['subject'],
                "title" => $r['name'],
                "type" => $r['type'],
                "name" => "timeout",
                "image" => addslashes(file_get_contents($file['tmp_name'])),
                "mime" => $file['type'],
                "added_by" => $user['id'],
            ];
            $db->insert("instructions", $data);
            header("Location: /{$r['path']}");
        }

        break;
    case "marking-add":

        $section = [
            "subject" => $r['subject'],
            "type" => $r['type'],
            "title" => $r['title'],
            "segment" => $r['segment'],
            "section" => $r['section'],
            "qst" => $r['qst'],
            "qnd" => $r['qnd'],
            "passing" => $r['passing'],
            "total" => $r['total'],
            "added_by" => $_SESSION['user']['id'],
        ];

        $db->insert("marking", $section);
        header("Location: " . $_SERVER['REQUEST_URI']);
        break;
    case "marking":

        unset($r["path"]);
        unset($r['action']);
        foreach ($r as $key => $value) {
            # code...
            if (strstr($key, "mQ")) {

                $marks = [
                    "type" => $path[4],
                    "course" => $path[2],
                    "title" => $path['3'],
                    "que" => $key,
                    "marks" => $value,
                    "added_from" => $_SERVER['REMOTE_ADDR'],
                ];
                $db->insert("marking_subj", $marks);
            }
        }
        header("Location: /exam/answers/" . $path[2] . "/" . $path[3] . "/" . $path[4]);
        break;
    case "add-ans-img":
        $file = $_FILES['page'];
        $mime = explode("/", $file['type']);
        // expolode("")
        if ($mime[0] == "image") {

            $data = [
                "category" => $r['category'],
                "subject" => $r['subject'],
                "title" => $r['name'],
                // "type" => $r['type'],
                "filename" => $file['name'],
                "image" => addslashes(file_get_contents($file['tmp_name'])),
                "mime" => $file['type'],
                "added_from" => $_SERVER['REMOTE_ADDR'],
            ];
            $db->debug(1);
            print_r(

                $db->insert("subans", $data));
        }
        break;
    case "upload-attempt-img":

        $file = $_FILES['page'];
        $mime = explode("/", $file['type']);
        if ($mime[0] == 'image') {

            $p = explode("/", $r['path']);
            $exam = $db->select("exam_permission", "*", "where id={$p[3]}")[0] ?? [];
            $attempt = [
                "student" => $user['id'],
                "exam" => $exam['id'],
                "category" => $r['category'],
                "image" => addslashes(file_get_contents($file['tmp_name'])),
                "mime" => $file['type'],
                "added_from" => $_SERVER['REMOTE_ADDR'],
            ];
            $db->insert("attempt", $attempt);
        } else {
            header("content-type: application/json");
            echo json_encode(['error' => "File type not supported. Please use JPG or PNG format only"]);
        }
        //include "mail.php";
        break;
    case 'rename-book':
        $p = explode("/", $r['path']);
        $db->query("update book1 set name='{$r['name']}' where name like '{$r['oldname']}' and subject={$p[1]}");
        header("Location: /");
        break;
    case "update-marks":
        $qp = [
            "exam" => $r['QP'],
            "qno" => $r['qno'],
            "marks" => $r['marks'],
            "comment" => $r['comments'],
            "added_by" => $user['id'],
        ];
        $db->insert("eval", $qp);
        header("Location: /eval/{$r['QP']}");
        break;
    case "commit-marks":
        $marks = $db->select("eval", "sum(marks) as total", "where exam={$r['QP']}")[0]['total'] ?? 0;
        $marks += $db->select("objatt", "marks", "where exam={$r['QP']}")[0]['marks'] ?? 0;
        $info = [
            "response" => $db->select("eval", "qno,marks,comment", "where exam={$r['QP']}"),
            "marks" => $marks,
        ];
        $db->update("exam_permission", $info, "where id={$r['QP']}");
        header("Location: /results/list");
        break;
    default:
        $db->insert("unknown_action", [
            "action" => $r['action'],
            "server" => $_SERVER,
            "session" => $_SESSION,
            "request" => $r,
        ]);
        // header("Location: /".$r['path']);
        header("content-type: text/txt");
        print_r($r);
        break;
}
die("");
